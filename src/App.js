import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Items from "./components/pages/Items";
import NavBar from "./components/nabvar/NavBar";
import { Helmet } from "react-helmet";

function App() {
  return (
    <>
      <Router>
        <Helmet>
          <title>lingokids</title>
          <meta name="description" content="get info for all your favorite toys"/>
        </Helmet>
        <NavBar />
        <div className="pages">
          <Switch>
            <Route path="/" component={Items} exact />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
