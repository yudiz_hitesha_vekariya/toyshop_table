import React from "react";

const GlobalFilter = ({ filter, setFilter }) => {
  return (
    <span
      style={{
        textAlign: "center",
        marginLeft: "18%",
        fontSize: "20px",
      }}
    >
      Search:{""}
      <input
        style={{
          width: "40%",
          border: "3px solid rgb(105, 206, 219)",
          padding: "5px",
          marginTop: "1%",
        }}
        value={filter || ""}
        onChange={(e) => setFilter(e.target.value)}
      />
    </span>
  );
};

export default GlobalFilter;
