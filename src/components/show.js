export const COLUMNS = [
  {
    Header: "id",
    accessor: "id",
  },
  {
    Header: " Name",
    accessor: "Name",
  },
  {
    Header: "Price",
    accessor: "Price",
  },
  {
    Header: "color",
    accessor: "color",
  },
  {
    Header: "Discription",
    accessor: "Discription",
  },
  {
    Header: "Size",
    accessor: "Size",
  },
];
