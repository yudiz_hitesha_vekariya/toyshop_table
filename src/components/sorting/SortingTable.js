import React, { useMemo } from "react";
import {
  useTable,
  useSortBy,
  useGlobalFilter,
  usePagination,
} from "react-table";
import MOCK_DATA from "../../data/MOCK_DATA.json";
import { COLUMNS } from "../show";
import "../pages/Basictable.css";
import GlobalFilter from "../GlobalFilter";

const SortingTable = () => {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => MOCK_DATA, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    prepareRow,
    state,
    setGlobalFilter,
  } = useTable({ columns, data }, useGlobalFilter, useSortBy, usePagination);
  const { globalFilter } = state;

  return (
    <>
      <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroups) => (
            <tr {...headerGroups.getHeaderGroupProps()}>
              {headerGroups.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render("Header")}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? " 🔽"
                        : " 🔼"
                      : ""}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((ROW) => {
            prepareRow(ROW);
            return (
              <tr {...ROW.getRowProps()}>
                {ROW.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}> {cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      <div>
        <button
          style={{
            fontSize: "20px",
            position: "sticky",
            border: "3px solid rgb(105, 206, 219)",
            padding: "5px",
            margin: "5px",
          }}
          onClick={() => previousPage()}
        >
          previous
        </button>
        <button
          style={{
            fontSize: "20px",
            position: "sticky",
            border: "3px solid rgb(105, 206, 219)",
            padding: "5px",
            margin: "5px",
          }}
          onClick={() => nextPage()}
        >
          Next
        </button>
      </div>
    </>
  );
};

export default SortingTable;
