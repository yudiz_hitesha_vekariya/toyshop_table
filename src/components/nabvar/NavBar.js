import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";

function NavBar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  return (
    <>
      <nav className="navbar">
        <div className="nav-container">
          <NavLink exact to="/" className="nav-logo">
            <img
              className="logoimg"
              src="https://wp-cdn.lingokids.com/wp-content/uploads/2020/08/logo.svg"
              alt=""
            ></img>
          </NavLink>

          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                TOYS
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}
export default NavBar;
