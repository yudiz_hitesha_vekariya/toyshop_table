import React from "react";
import SortingTable from "../sorting/SortingTable";

const Items = () => {
  return (
    <div>
      <SortingTable />
    </div>
  );
};

export default Items;
